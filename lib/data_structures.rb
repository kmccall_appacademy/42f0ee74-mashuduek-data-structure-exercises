# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  return true if arr == arr.sort
  false
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vows = 'aeiouAEIOU'
  count = 0
  str.each_char { |l| count += 1 if vows.include?(l) }
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.delete('AEIOUaeiou')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.split(//).sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  letters = []
  str.each_char do |let|
    return true if letters.include?(let.downcase)
    letters << let.downcase
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  arr.insert(6,'-')
  arr.insert(3,' ')
  arr.insert(3,')')
  arr.insert(0,'(')
  arr.join
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  arr = str.split(//)
  arr.delete(',')
  arr.map! {|n| n.to_i}
  arr.max - arr.min
end

#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
require 'byebug'
def my_rotate(arr, offset=1)
  # debugger
  to_off = offset % arr.length
  to_drop = arr.drop(to_off)
  final = arr - to_drop
  to_drop + final

end
